# EMSA Unit Workload Application
Application that calculates individual unit workload and displays the results into easy to read formats using chartJS and HTML tables. 
Front End: HTML5, CSS3(SCSS), JS/JQuery --
Back End: Flask/Python/MySQL --
What the Unit Workload Number represents: its a representation of each individual unit (ambulance) workload. Parameters that are used are Arrivals, Task Time, Percentage Of Shift Worked, And Post Time.

# WORKFLOW
Raspberry Pi 4
1. Runs Chromium Browser with JS/JQuery script in browser to download CSV file from FirstWatch Live Workload Report every 2.5 minutes.
2. Python/Flask server running on pi looks for CSV file in downloads folder. 
3. Python then handles csv file by filtering required data, organizing into list, then saving that data to MySQL server hosted on PythonAnywhere.com.
4. CSV then deleted, and script continues to run, listening for another csv file.
5. This system is unstable at the moment. Many factors play into the server going down, anywhere from First Watch not reloading properly, to MySQL database timing out.
6. Error handling repsonsible for sending email in the event of system failure, to allow for remote accessing the pi for troubleshooting.

PythonAnywhere
1. Pulls required data from MySQL server, runs required variables through algorithm to achieve workload number and current thresold for unit.
2. Sends all data to browser upon request.
3. JQuery then handles all received data to be distributed among charts and tables.

Application Functonality
1. Is password protected, to allow only verified users to  have access. User Role can only view dashboard to see current workload data. Admin Role allows for Adding/Updating/Deleting of users.

