from flask import Flask, Blueprint
from unit_workload.user.api.views import fetch_api_user
from unit_workload.admin.api.views import fetch_api_admin
from unit_workload.user.site.views import user_site
from unit_workload.admin.site.views import admin_site
from unit_workload.extensions import db, login_manager, cors

def create_app():
    temp_folder = "templates"

    app = Flask(__name__, template_folder=temp_folder)

    app.config.from_pyfile("config.py")

    app.register_blueprint(fetch_api_user, url_prefix="/api/user")
    app.register_blueprint(fetch_api_admin, url_prefix="/api/admin")
    app.register_blueprint(user_site)
    app.register_blueprint(admin_site)

    db.init_app(app)
    login_manager.init_app(app)
    cors.init_app(app)

    # with app.app_context():
    #     db.create_all()

    return app
