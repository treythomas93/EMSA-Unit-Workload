# imports
from flask import Blueprint, request, jsonify, abort, session
from flask_cors import cross_origin
from unit_workload.models import User, Live_Workload, Hour_By_Hour_Workload
from unit_workload.extensions import db
from unit_workload.error import error_handler, get_current_datetime
from unit_workload.calc import calculate_workload
from flask_login import current_user
from werkzeug.security import generate_password_hash, check_password_hash

from datetime import timedelta, datetime
from random import randint

fetch_api_admin = Blueprint("fetch_api_admin", __name__)

# fetch users for admin panel
@fetch_api_admin.route("/fetch_users", methods=["GET"])
@cross_origin()
def fetch_users():
    try:
        if request.method == "GET":
            users = [{"First_Name": user.First_Name,
                      "Last_Name": user.Last_Name,
                      "Username": user.Username,
                      "User_ID": user.id,
                      "Role": user.User_Role,
                      "Date_Added": user.Date_Added} for user in User.query.all()]
            return jsonify(users)
        else:
            # method not allowed error
            return abort(405)
    except Exception as e:
        error_handler("Fetch Users", "Low", e)
        return e


@fetch_api_admin.route("/add_user", methods=['POST'])
@cross_origin()
def add_user():
    try:
        first_name = request.form["first_name"]
        last_name = request.form["last_name"]
        username = request.form["username"]
        password = request.form["password"]
        confirm_password = request.form["confirm_password"]
        role = request.form["role"]
        check_user = User.query.filter_by(First_Name=first_name.capitalize(), Last_Name=last_name.capitalize()).first()
        if check_user:
            return jsonify("ERROR: User already in database")
        else:
            if first_name and last_name and username and password and confirm_password and role:
                if password == confirm_password:
                    dt = get_current_datetime(None)
                    password = generate_password_hash(password)
                    add = User(first_name.capitalize(),
                            last_name.capitalize(), username, password, role, dt)
                    db.session.add(add)
                    db.session.commit()
                    db.session.close()
                    return jsonify("User Added Succesfully!")
                else:
                    return jsonify("ERROR: Passwords Do Not Match!")
            else:
                return jsonify("ERROR: Empty Fields!")
    except Exception as e:
        error_handler("ADD USER", "Low", e)
        dt = get_current_datetime(None)
        return "ERROR(ADD USER): {} - Datetime: {}\n".format(e, dt)


@fetch_api_admin.route("/edit_user", methods=['POST'])
@cross_origin()
def edit_user():
    try:
        first_name = request.form["first_name"]
        last_name = request.form["last_name"]
        username = request.form["username"]
        password = request.form["password"]
        confirm_password = request.form["confirm_password"]
        user_id = request.form["user_id"]
        role = request.form["role"]
        # print(f"{first_name} {last_name} {username} {password} {confirm_password} {user_id} {role}")
        user = User.query.filter_by(id=user_id).first()
        if first_name and last_name and username and password and confirm_password and role and user_id:
            if password == confirm_password:
                if user:
                    user.First_Name = first_name.capitalize()
                    user.Last_Name = last_name.capitalize()
                    user.Username = username
                    user.Password = generate_password_hash(password)
                    user.User_Role = role
                    db.session.commit()
                    db.session.close()
                return jsonify("User Updated Succesfully!")
            else:
                return jsonify("ERROR: Passwords Do Not Match!")
        else:
            return jsonify("ERROR: Empty Fields")
    except Exception as e:
        error_handler("EDIT USER", "Low", e)
        dt = get_current_datetime(None)
        return "ERROR(EDIT USER): {} - Datetime: {}\n".format(e, dt)


@fetch_api_admin.route("/delete_user", methods=['POST'])
@cross_origin()
def delete_user():
    try:
        id = request.form["user_id"]
        password = request.form["password"]
        if check_password_hash(current_user.Password, password):
            user = User.query.filter_by(id=id).first()
            if user:
                db.session.delete(user)
                db.session.commit()
                db.session.close()
            msg = "User Deleted Successfully!"
            return jsonify(msg)
        else:
            msg = "ERROR: Password Incorrect!"
            return jsonify(msg) 
    except Exception as e:
        error_handler("DELETE USER", "Low", e)
        dt = get_current_datetime(None)
        return "ERROR(DELETE USER): {} - Datetime: {}\n".format(e, dt)
