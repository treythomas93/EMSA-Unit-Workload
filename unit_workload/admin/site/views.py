from flask import Blueprint, render_template, request, session, redirect, url_for
from flask_login import login_user, logout_user, login_required, current_user
from unit_workload.models import User, Live_Workload, Hour_By_Hour_Workload
from unit_workload.extensions import db, login_manager
from unit_workload.error import error_handler, get_current_datetime
from datetime import timedelta, datetime

admin_site = Blueprint("admin_site", __name__)

@admin_site.route("/admin", methods=['GET', 'POST'])
@login_required
def admin():
    user = current_user.Username
    auth = True
    role = current_user.User_Role
    return render_template("admin.html", user=user, auth=auth, role=role)
