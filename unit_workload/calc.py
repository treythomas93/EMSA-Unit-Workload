from unit_workload.error import error_handler, get_current_datetime
import datetime
from datetime import datetime

"""
CALCULATE WORKLOAD
"""

def get_time_difference(sos):
    current_time = get_current_datetime("Time")
    s1 = sos
    s2 = str(current_time).split(" ")[-1]
    FMT = '%H:%M:%S'
    tdelta = datetime.strptime(s2, FMT) - datetime.strptime(s1, FMT)
    tdelta = str(tdelta).split(" ")[-1]
    sos = datetime.strptime(sos, "%H:%M:%S")
    hours_diff = tdelta[:4]
    hours_diff = hours_diff.replace(":", ".")
    return hours_diff


def calculate_workload(sos, task_time, arrivals, post_time):
    try:
        # GET DIFFERENCE BETWEEN SOS AND CURRENT TIME
        hours_diff = get_time_difference(sos)

        # PERCENTAGE OF SHIFT WORKED
        percentage_worked = round(float(hours_diff) / 12, 2)

        # CURRENT THRESHOLD PER ANY GIVEN TIME. IF ABOVE, THEN ASSUMED BUSIER THAN NORMAL.
        max_threshold = 1
        current_threshold = (max_threshold / 12) * round(float(hours_diff), 2)

        # POST TIME WILL TAKE AWAY FROM WORKLOAD NUMBER
        if arrivals > 0:
            max_task = 70
            true_task = (task_time / arrivals)
            task_dif = max_task + true_task

            # LOWER THE TASK TIME, THE HIGHER THE WORKLOAD
            w1 = round(((percentage_worked / task_dif)
                        * 100) + (arrivals / 10), 2)
            workload = w1 - (post_time / 100)
        else:
            workload = (current_threshold - (post_time / 100))

        return (round(workload, 2), round(current_threshold, 2))
    except Exception as e:
        error_handler("Calculate Workload", "Moderate", e)

