# imports
import os
import pymysql.cursors

from unit_workload.connection import connection

tunnel = connection()

SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://Username:Password@127.0.0.1:{}/Username$DatabaseName'.format(tunnel.local_bind_port)
SQLALCHEMY_BINDS = ({'User': 'mysql+pymysql://Username:Password@127.0.0.1:{}/Username$DatabaseName'.format(tunnel.local_bind_port),
                    'Hour By Hour Workload': 'mysql+pymysql://Username:Password@127.0.0.1:{}/Username$DatabaseName'.format(tunnel.local_bind_port),
                    'Continuous Workload': 'mysql+pymysql://Username:Password@127.0.0.1:{}/Username$DatabaseName'.format(tunnel.local_bind_port)})
SECRET_KEY = os.urandom(24)
SQLALCHEMY_POOL_RECYCLE = 280
SQLALCHEMY_POOL_TIMEOUT = 20
DEBUG = True
