# imports
import sshtunnel

from unit_workload.error import error_handler

# handle connection to mysql server via sshtunneling
def connection():
    try:
        sshtunnel.SSH_TIMEOUT = 5.0
        sshtunnel.TUNNEL_TIMEOUT = 5.0
        tunnel = sshtunnel.SSHTunnelForwarder(
            ('ssh.pythonanywhere.com'), ssh_username='TreyThomas673', ssh_password='Aubree673',
            remote_bind_address=(
                'TreyThomas673.mysql.pythonanywhere-services.com', 3306)
        )

        tunnel.start()

        return tunnel
    except Exception as e:
        error_handler("Connection", e)