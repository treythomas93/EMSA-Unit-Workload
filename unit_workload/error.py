# imports
from datetime import datetime, timedelta
import pytz
import smtplib
import ssl
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import os

error_path = os.getcwd()

# get current datetime
def get_current_datetime(type):
    dt = datetime.now(tz=pytz.UTC).replace(microsecond=0)
    dt_central = dt.astimezone(pytz.timezone('US/Central'))
    dt = datetime.strftime(dt_central, "%m/%d/%y %H:%M:%S")
    if type == "Time":
        dt = dt.split()[1]
    elif type == "Date":
        dt = dt.split()[0]
    return dt

# send email if priority is HIGH
def send_it(type, e, dt):
    try:
        # port = 587  # For SSL
        # smtp_server = "smtp.gmail.com"
        sender_email = "Email@gmail.com"  # Enter your address
        receiver_email = "Email@gmail.com"  # Enter receiver address
        password = 'Password'
        message = MIMEMultipart("alternative")
        message["Subject"] = "Workload Server Error"
        message["From"] = sender_email
        message["To"] = receiver_email

        text = "ERROR: {} - {} - {}".format(type, e, dt)

        message.attach(MIMEText(text, "plain"))

        # Create secure connection with server and send email
        context = ssl.create_default_context()
        with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context) as server:
            server.login(sender_email, password)
            server.sendmail(
                sender_email, receiver_email, message.as_string()
            )
        print("Error Message Sent!")
    except Exception as e:
        print("ERROR(SEND EMAIL): {} - Datetime: {}\n".format(e, dt))

# handle errors
def error_handler(type, priority, e):
    dt = get_current_datetime(None)
    try:
        if priority == "High":
            send_it(type, e, dt)
        with open("" + error_path + "/error_log.txt", "a") as f:
            f.write("ERROR({}): {} - Datetime: {}\n".format(type, e, dt))
        return "Error Logged"
    except Exception as e:
        print("ERROR(ERROR HANDLER): {} - Datetime: {}\n".format(e, dt))
