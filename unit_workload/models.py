from flask_login import UserMixin
from unit_workload.extensions import db

class Live_Workload(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    updated_at = db.Column(db.String(10))
    crew_member_one = db.Column(db.String(30))
    crew_member_two = db.Column(db.String(30))
    unit = db.Column(db.String(10))
    sos = db.Column(db.String(10))
    task_time = db.Column(db.Integer())
    arrivals = db.Column(db.Integer())
    post_time = db.Column(db.Integer())
    trending = db.Column(db.String(30))
    last_post = db.Column(db.String(10))

    def __init__(self, updated_at, crew_member_one, crew_member_two, unit, sos, task_time, arrivals, post_time, trending, last_post):
        self.updated_at = updated_at
        self.crew_member_one = crew_member_one
        self.crew_member_two = crew_member_two
        self.unit = unit
        self.sos = sos
        self.task_time = task_time
        self.arrivals = arrivals
        self.post_time = post_time
        self.trending = trending
        self.last_post = last_post

class Hour_By_Hour_Workload(db.Model):
    __bind_key__ = "Hour By Hour Workload"
    id = db.Column(db.Integer(), primary_key=True)
    date = db.Column(db.String(50))
    workload = db.Column(db.Float())
    temperature = db.Column(db.Float())
    weather = db.Column(db.String(50))

    def __init__(self, date, workload, temperature, weather):
        self.date = date
        self.workload = workload
        self.temperature = temperature
        self.weather = weather

class User(db.Model, UserMixin):
    __bind_key__ = "User"
    id = db.Column(db.Integer(), primary_key=True)
    First_Name = db.Column(db.String(50))
    Last_Name = db.Column(db.String(50))
    Username = db.Column(db.String(50), unique=True)
    Password = db.Column(db.String(255))
    User_Role = db.Column(db.String(50))
    Date_Added = db.Column(db.String(50))

    def __init__(self, First_Name, Last_Name, Username, Password, User_Role, Date_Added):
        self.First_Name = First_Name
        self.Last_Name = Last_Name
        self.Username = Username
        self.Password = Password
        self.User_Role = User_Role
        self.Date_Added = Date_Added
