$(document).ready(function() {
  global_url = "http://127.0.0.1:5000/";

  // request users
  function request_users() {
    $(".manage-users-table tbody")
      .children()
      .remove();
    $.ajax({
      url: global_url + "api/admin/fetch_users",
      type: "GET",
      success: function(data) {
        for (var i = 0; i < data.length; i++) {
          var first_name = data[i].First_Name;
          var last_name = data[i].Last_Name;
          var name = first_name + " " + last_name;
          var username = data[i].Username;
          var user_id = data[i].User_ID;
          var user_role = data[i].Role;
          var date_created = data[i].Date_Added;
          // send to table
          $(".manage-users-table tbody").append(
            `
            
                <tr>
                    <td>` +
              name +
              `</td>
                    <td>` +
              username +
              `</td>
              <td>` +
              user_role +
              `</td>
                    <td>` +
              date_created +
              `</td>
              <td><button title="Edit User" value='` +
              user_id +
              `'><i class="fas fa-user-edit"></i></button></td>
              <td><button title="Delete User" value='` +
              user_id +
              `'><i class="fas fa-user-times"></i></button></td>
                </tr>

          `
          );
        }
      },
      error: function(request, error) {
        alert(error);
      }
    });
  }
  request_users();

  // handle add/edit/delete popup
  $(".manage-users-table").on("click", "button", function() {
    $(".popup-manager")
      .show()
      .children()
      .hide();
    var value = $(this).val();
    var name = $(this)
      .closest("tr")
      .children("td:nth-child(1)")
      .text()
      .split(" ");
    var first_name = name[0];
    var last_name = name[1];
    var username = $(this)
      .closest("tr")
      .children("td:nth-child(2)")
      .text();
    var role = $(this)
      .closest("tr")
      .children("td:nth-child(3)")
      .text();
    if ($(this).attr("title") == "Add User") {
      $(".add-user-popup").show();
    } else if ($(this).attr("title") == "Edit User") {
      $(".edit-user-popup").show();
      $(".edit-user-popup form input[name=user_id]").val(value);
      $(".edit-user-popup form input[name=first_name]").val(first_name);
      $(".edit-user-popup form input[name=last_name]").val(last_name);
      $(".edit-user-popup form input[name=username]").val(username);
      $(".edit-user-popup form input[name=password]").val(password);
      $(".edit-user-popup form select[name=role]").val(role);
    } else if ($(this).attr("title") == "Delete User") {
      $(".delete-user-popup").show();
      $(".delete-user-popup form input[name=user_id]").val(value);
    } else {
      $(".popup-manager")
        .hide()
        .children()
        .hide();
    }
  });

  // add user
  function add_user(data) {
    $.ajax({
      url: global_url + "api/admin/add_user",
      type: "POST",
      data: data,
      success: function(resp) {
        alert(resp);
        request_users();
        $("#add-user-form").each(function() {
          this.reset();
        });
      },
      error: function(request, error) {
        alert(error);
      }
    });
  }

  $("#add-user-form").on("click", "button", function(e) {
    e.preventDefault();
    data = {
      first_name: $(this)
        .parent("form")
        .children("input[name=first_name]")
        .val(),
      last_name: $(this)
        .parent("form")
        .children("input[name=last_name]")
        .val(),
      username: $(this)
        .parent("form")
        .children("input[name=username]")
        .val(),
      password: $(this)
        .parent("form")
        .children("input[name=password]")
        .val(),
      confirm_password: $(this)
        .parent("form")
        .children("input[name=confirm_password]")
        .val(),
      role: $(this)
        .parent("form")
        .children("select[name=role]")
        .val()
    };
    add_user(data);
  });

  // edit user
  function edit_user(data) {
    $.ajax({
      url: global_url + "api/admin/edit_user",
      type: "POST",
      data: data,
      success: function(resp) {
        alert(resp);
        request_users();
        $("#edit-user-form").each(function() {
          this.reset();
        });
      },
      error: function(request, error) {
        alert(error);
      }
    });
  }

  $("#edit-user-form").on("click", "button", function(e) {
    e.preventDefault();
    data = {
      first_name: $(this)
        .parent("form")
        .children("input[name=first_name]")
        .val(),
      last_name: $(this)
        .parent("form")
        .children("input[name=last_name]")
        .val(),
      username: $(this)
        .parent("form")
        .children("input[name=username]")
        .val(),
      password: $(this)
        .parent("form")
        .children("input[name=password]")
        .val(),
      confirm_password: $(this)
        .parent("form")
        .children("input[name=confirm_password]")
        .val(),
      role: $(this)
        .parent("form")
        .children("select[name=role]")
        .val(),
      user_id: $(this)
        .parent("form")
        .children("input[name=user_id]")
        .val()
    };
    edit_user(data);
  });

  // delete user
  function delete_user(data) {
    $.ajax({
      url: global_url + "api/admin/delete_user",
      type: "POST",
      data: data,
      success: function(resp) {
        alert(resp);
        request_users();
        $("#delete-user-form").each(function() {
          this.reset();
        });
      },
      error: function(request, error) {
        alert(error);
      }
    });
  }

  $("#delete-user-form").on("click", "button", function(e) {
    e.preventDefault();
    data = {
      password: $(this)
        .parent("form")
        .children("input[name=password]")
        .val(),
      user_id: $(this)
        .parent("form")
        .children("input[name=user_id]")
        .val()
    };
    delete_user(data);
  });
});
