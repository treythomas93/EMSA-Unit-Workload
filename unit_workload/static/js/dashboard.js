$(document).ready(function() {
  global_url = "http://127.0.0.1:5000/";

  setInterval(function() {
    request_workload();
  }, 300000);

  sort_workload = [];
  sort_threshold = [];

  sort_ratio = [];
  sort_post = [];
  // request workload data via ajax get
  function request_workload() {
    try {
      $.ajax({
        url: global_url + "api/user/fetch_live_workload",
        type: "GET",
        success: function(data) {
          for (var i = 0; i < data.length; i++) {
            var updated_at = data[i].Updated_At;
            var unit = data[i].Unit;
            var threshold = data[i].Threshold;
            var workload = data[i].Workload;
            var arrivals = data[i].Arrivals;
            var task_time = data[i].Task_Time;
            var server_status = data[i].Server_Status;
            if (arrivals > 0) {
              var task_time = Math.round(task_time / arrivals);
            }
            var post_time = data[i].Post_Time;
            var hours = Math.floor(post_time / 60);
            var minutes = post_time % 60;

            if (minutes < 10) {
              var minutes = "0" + minutes;
            }

            var post_time = hours + ":" + minutes;
            var trending = data[i].Trending;
            var last_post = data[i].Last_Post;
            var ratio = (workload - threshold).toFixed(2);

            // sort workload
            sort_workload.push([
              unit,
              threshold,
              workload,
              ratio,
              arrivals,
              task_time,
              post_time,
              trending,
              last_post
            ]);

            // sort threshold
            sort_threshold.push([unit, threshold, workload]);

            // sort ratio
            sort_ratio.push([unit, ratio]);

            var post_diff = get_time_diff(last_post);

            // sort post
            sort_post.push([unit, last_post, post_diff]);
          }

          // sort workload descending order
          sort_workload.sort(function(a, b) {
            return b[2] - a[2];
          });

          // sort threshold descending order
          sort_threshold.sort(function(a, b) {
            return b[1] - a[1];
          });

          // sort ratio descending order
          sort_ratio.sort(function(a, b) {
            return b[1] - a[1];
          });

          // sort post descending order
          sort_post.sort(function(a, b) {
            return b[2] - a[2];
          });

          // make workload bar chart
          update_workload_chart(updated_at);

          // make workload table
          make_workload_table();

          // get total amount of units per table
          get_total_units();

          // get top 3's
          get_top_threes();

          check_server_status(server_status);

          // clear sort arrays
          sort_workload.length = 0;
          sort_threshold.length = 0;
          sort_ratio.length = 0;
          sort_post.length = 0;
        },
        error: function(request, error) {
          alert(error);
        }
      });
    } catch (error) {
      console.log(error);
    }
  }
  request_workload();

  function check_server_status(server_status) {
    if (server_status < 10) {
      $("#server-status")
        .text("Online")
        .css({
          background: "yellow",
          color: "var(--primary)",
          padding: "0 1em",
          "border-radius": "10px"
        });
    } else {
      $("#server-status")
        .text("Offline")
        .css({
          background: "red",
          color: "var(--secondary)",
          padding: "0 1em",
          "border-radius": "10px"
        });
    }
  }

  // get time diff from last post
  function get_time_diff(last_post) {
    var dt = new Date();
    var time =
      ("0" + dt.getHours()).slice(-2) + ":" + ("0" + dt.getMinutes()).slice(-2);

    var diff =
      new Date("Aug 08 2012 " + time + "") -
      new Date("Aug 08 2012 " + last_post + "");
    diff_time = diff / (60 * 60 * 1000);
    return diff_time;
  }

  // make workload bar chart
  function make_workload_bar_chart() {
    var chart_data = {
      labels: [],
      datasets: [
        {
          label: "Current Threshold",
          backgroundColor: [],
          data: []
        },
        {
          label: "Current UWN",
          backgroundColor: [],
          data: []
        },
        {
          label: "Max Threshold(1.0) Beta",
          fill: false,
          borderColor: "blue",
          backgroundColor: "blue",
          data: [],

          // Changes this dataset to become a line
          type: "line"
        },
        {
          label: "Surpassing Current Threshold",
          backgroundColor: "yellow"
        },
        {
          label: "Surpassing Max Threshold",
          backgroundColor: "red"
        },
        {
          label: "Current Workload",
          backgroundColor: "#2F607B"
        }
      ]
    };

    var chart_options = {
      maintainAspectRatio: false,
      scales: {
        xAxes: [
          {
            stacked: true,
            ticks: {
              fontSize: 12,
              fontColor: "white"
            },
            barPercentage: 0.7,
            gridLines: {
              color: "rgba(255, 255, 255, 0.25)"
            }
          }
        ],
        yAxes: [
          {
            stacked: false,
            ticks: {
              fontSize: 12,
              beginAtZero: true,
              fontColor: "white"
            },
            gridLines: {
              color: "rgba(255, 255, 255, 0.25)"
            }
          }
        ]
      },
      animation: false,
      elements: {
        point: {
          radius: 0
        }
      },
      title: {
        display: true,
        text: "Current Unit Workload",
        fontSize: 15,
        fontColor: "white"
      },
      legend: {
        labels: {
          filter: function(item, chart) {
            // Logic to remove a particular legend item goes here
            return item.text == null || !item.text.includes("Current UWN");
          },
          fontColor: "white"
        }
      }
    };

    var ctx = document
      .getElementById("current-workload-bar-chart")
      .getContext("2d");

    barChart = new Chart(ctx, {
      // The type of chart we want to create
      type: "bar",

      // The data for our dataset
      data: chart_data,

      // Configuration options go here
      options: chart_options
    });
  }
  make_workload_bar_chart();

  function update_workload_chart(updated_at) {
    // remove data
    barChart.data.labels = [];
    barChart.data.datasets[0].data = [];
    barChart.data.datasets[1].data = [];
    barChart.data.datasets[2].data = [];
    barChart.data.datasets[1].backgroundColor = [];
    barChart.data.datasets[0].backgroundColor = [];
    barChart.update();

    for (var i = 0; i < sort_threshold.length; i++) {
      var unit = sort_threshold[i][0];
      if ($.inArray(unit, barChart.data.labels) == -1 && unit !== undefined) {
        var current_threshold = sort_threshold[i][1];
        var current_workload = sort_threshold[i][2];
        if (current_workload >= 1) {
          barChart.data.datasets[1].backgroundColor.push("red");
          barChart.data.datasets[0].backgroundColor.push("#2E4E64");
        } else if (current_workload > current_threshold) {
          barChart.data.datasets[1].backgroundColor.push("yellow");
          barChart.data.datasets[0].backgroundColor.push("#2E4E64");
        } else {
          barChart.data.datasets[1].backgroundColor.push("#32A9D9");
          barChart.data.datasets[0].backgroundColor.push(
            "rgba(46, 78, 100, 0.8)"
          );
        }

        if (current_workload < 0) {
          current_workload = 0;
        }

        barChart.data.labels.push(unit);
        barChart.data.datasets[0].data.push(current_threshold);
        barChart.data.datasets[1].data.push(current_workload);
        barChart.data.datasets[2].data.push(1);
        barChart.update();
      }
    }

    $("#updated-at").text(updated_at);
  }

  // make workload table function
  function make_workload_table() {
    $("tbody")
      .children()
      .remove();
    for (var i = 0; i < sort_workload.length; i++) {
      var unit = sort_workload[i][0];
      var threshold = sort_workload[i][1];
      var workload = sort_workload[i][2];
      var ratio = sort_workload[i][3];
      var arrivals = sort_workload[i][4];
      var task_time = sort_workload[i][5];
      var post_time = sort_workload[i][6];
      var trending = sort_workload[i][7];
      var last_post = sort_workload[i][8];
      // check if threshold is less than or equal to 1.0
      if (threshold <= 1) {
        // highlight within one/two hours of EOS
        // 2 hours = 0.83
        // 1 hour = 0.92
        if (threshold >= 0.83 && threshold < 0.92) {
          var highlight = "lightblue";
          var font_color = "black";
        } else if (threshold >= 0.92) {
          var highlight = "lightgreen";
          var font_color = "black";
        } else {
          var highlight = "";
          var font_color = "white";
        }

        // check if unit above max
        if (workload >= 1) {
          // append to table
          $(".above-max-table tbody").append(
            `

            <tr style='background: ` +
              highlight +
              `; color: ` +
              font_color +
              `'>
                <td>` +
              unit +
              `</td>
                <td>` +
              workload +
              `</td>
                <td>` +
              threshold +
              `</td>
                <td>` +
              ratio +
              `</td>
                <td>` +
              arrivals +
              `</td>
                <td>` +
              task_time +
              `</td>
                <td>` +
              post_time +
              `</td>
                <td>` +
              last_post +
              `</td>
                <td>` +
              trending +
              `</td>
            </tr>

        `
          );
        }
        // check if unit above current threshold
        else if (workload < 1 && workload > threshold) {
          // append to table
          $(".above-current-table tbody").append(
            `

            <tr style='background: ` +
              highlight +
              `; color: ` +
              font_color +
              `'>
                <td>` +
              unit +
              `</td>
                <td>` +
              workload +
              `</td>
                <td>` +
              threshold +
              `</td>
                <td>` +
              ratio +
              `</td>
                <td>` +
              arrivals +
              `</td>
                <td>` +
              task_time +
              `</td>
                <td>` +
              post_time +
              `</td>
                <td>` +
              last_post +
              `</td>
                <td>` +
              trending +
              `</td>
            </tr>

        `
          );
        }
        // finally if unit neither above, send to other table
        else {
          // append to table
          $(".other-table tbody").append(
            `

            <tr style='background: ` +
              highlight +
              `; color: ` +
              font_color +
              `'>
                <td>` +
              unit +
              `</td>
                <td>` +
              workload +
              `</td>
                <td>` +
              threshold +
              `</td>
                <td>` +
              ratio +
              `</td>
                <td>` +
              arrivals +
              `</td>
                <td>` +
              task_time +
              `</td>
                <td>` +
              post_time +
              `</td>
                <td>` +
              last_post +
              `</td>
                <td>` +
              trending +
              `</td>
            </tr>

        `
          );
        }
      }
    }
  }

  // get total units per table
  function get_total_units() {
    var above_max_count = $(".above-max-table tbody").children().length;
    var above_current_count = $(".above-current-table tbody").children().length;
    var other_count = $(".other-table tbody").children().length;

    // apply to span
    $("#above-max-count").text("(" + above_max_count + " Units)");
    $("#above-current-count").text("(" + above_current_count + " Units)");
    $("#other-count").text("(" + other_count + " Units)");
  }

  // top 3's
  function get_top_threes() {
    // top 3 by workload
    for (var i = 0; i < 3; i++) {
      var unit = sort_workload[i][0];
      var workload = sort_workload[i][2];

      $(".busiest-by-workload tbody").append(
        `
          
            <tr>
                <td>` +
          unit +
          `</td>
                <td>` +
          workload +
          `</td>
            </tr>

          `
      );
    }

    // top 3 by ratio
    for (var i = 0; i < 3; i++) {
      var unit = sort_ratio[i][0];
      var ratio = sort_ratio[i][1];

      $(".busiest-by-ratio tbody").append(
        `
            
              <tr>
                  <td>` +
          unit +
          `</td>
                  <td>` +
          ratio +
          `</td>
              </tr>
  
            `
      );
    }

    // top 3 by workload
    for (var i = 0; i < 3; i++) {
      var unit = sort_post[i][0];
      var last_post = sort_post[i][1];

      $(".longest-without-post tbody").append(
        `
            
              <tr>
                  <td>` +
          unit +
          `</td>
                  <td>` +
          last_post +
          `</td>
              </tr>
  
            `
      );
    }
  }

  //////////////////////////
  check = true;
  setInterval(function() {
    var dt = new Date();
    var time =
      ("0" + dt.getHours()).slice(-2) + ":" + ("0" + dt.getMinutes()).slice(-2);
    if ($.inArray(time, hours) !== -1 && check) {
      check = false;
      setTimeout(function() {
        request_hour_by_hour_workload();
      }, 10000);
    } else if ($.inArray(time, hours) == -1 && check == false) {
      check = true;
    }
  }, 1000);

  hours = [
    "00:00",
    "01:00",
    "02:00",
    "03:00",
    "04:00",
    "05:00",
    "06:00",
    "07:00",
    "08:00",
    "09:00",
    "10:00",
    "11:00",
    "12:00",
    "13:00",
    "14:00",
    "15:00",
    "16:00",
    "17:00",
    "18:00",
    "19:00",
    "20:00",
    "21:00",
    "22:00",
    "23:00"
  ];

  // request hour by hour workload
  function request_hour_by_hour_workload() {
    try {
      $.ajax({
        url: global_url + "api/user/fetch_hour_by_hour_workload",
        type: "GET",
        success: function(data) {
          sift_hour_by_hour(data);
        },
        error: function(request, error) {
          alert(error);
        }
      });
    } catch (error) {
      alert(error);
    }
  }
  request_hour_by_hour_workload();

  function sift_hour_by_hour(data) {
    var today = data.Today;
    var yesterday = data.Yesterday;

    update_hour_by_hour_chart(today, yesterday);
  }

  function make_hour_by_hour_line_chart() {
    var chart_data = {
      labels: [],
      datasets: [
        {
          label: "Today",
          backgroundColor: "#32A9D9",
          data: [],
          fill: false,
          borderColor: "#32A9D9"
        },
        {
          label: "Yesterday",
          backgroundColor: "rgba(255, 0, 0, 0.25)",
          data: [],
          fill: "-1",
          borderColor: "red"
        }
      ]
    };

    var chart_options = {
      maintainAspectRatio: false,
      scales: {
        xAxes: [
          {
            ticks: {
              fontSize: 12,
              fontColor: "white"
            },
            gridLines: {
              color: "rgba(255, 255, 255, 0.25)"
            }
          }
        ],
        yAxes: [
          {
            ticks: {
              fontSize: 12,
              beginAtZero: true,
              fontColor: "white"
            },
            gridLines: {
              color: "rgba(255, 255, 255, 0.25)",
              zeroLineColor: "rgba(255, 255, 255, 0.25)"
            }
          }
        ]
      },
      animation: false,
      title: {
        display: true,
        text: "Hour By Hour Workload",
        fontSize: 15,
        fontColor: "white"
      },
      legend: {
        labels: {
          fontColor: "white"
        }
      }
    };

    var ctx = document
      .getElementById("hour-by-hour-line-chart")
      .getContext("2d");

    lineChart = new Chart(ctx, {
      // The type of chart we want to create
      type: "line",

      // The data for our dataset
      data: chart_data,

      // Configuration options go here
      options: chart_options
    });
  }

  make_hour_by_hour_line_chart();

  function update_hour_by_hour_chart(today, yesterday) {
    // remove data
    lineChart.data.labels = [];
    lineChart.data.datasets[0].data = [];
    lineChart.data.datasets[1].data = [];
    lineChart.update();

    // loop yesterday
    for (var i = 0; i < yesterday.length; i++) {
      lineChart.data.labels.push(yesterday[i].Time);
      lineChart.data.datasets[1].data.push(yesterday[i].Workload);
      lineChart.update();
    }

    // loop today
    for (var i = 0; i < today.length; i++) {
      lineChart.data.datasets[0].data.push(today[i].Workload);
      lineChart.update();
    }
  }
});
