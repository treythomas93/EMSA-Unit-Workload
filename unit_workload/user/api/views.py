# imports
from flask import Blueprint, request, jsonify, abort
from flask_cors import cross_origin
from unit_workload.models import User, Live_Workload, Hour_By_Hour_Workload
from unit_workload.extensions import db
from unit_workload.error import error_handler, get_current_datetime
from unit_workload.calc import calculate_workload

from datetime import timedelta, datetime

fetch_api_user = Blueprint("fetch_api_user", __name__)

# fetch workload data for dashboard
@fetch_api_user.route("/fetch_live_workload", methods=["GET"])
@cross_origin()
def fetch_live_workload():
    try:
        if request.method == "GET":
            data = []
            for unit in Live_Workload.query.all():
                workload = calculate_workload(
                    unit.sos, unit.task_time, unit.arrivals, unit.post_time)
                server_status = datetime.strptime(get_current_datetime("Time"), "%H:%M:%S") - datetime.strptime(unit.updated_at, "%H:%M:%S")
                server_status = str(server_status)[3:4]
                obj = {"Unit": unit.unit,
                       "Task_Time": unit.task_time,
                       "Arrivals": unit.arrivals,
                       "Post_Time": unit.post_time,
                       "Trending": unit.trending,
                       "Last_Post": unit.last_post,
                       "Updated_At": unit.updated_at,
                       "Workload": workload[0],
                       "Threshold": workload[1],
                       "Server_Status" : server_status}
                data.append(obj)
            return jsonify(data)
        else:
            # method not allowed error
            return abort(405)
    except Exception as e:
        error_handler("Fetch Workload", "Low", e)
        return e

# fetch hour by hour workload for dashboard
@fetch_api_user.route("/fetch_hour_by_hour_workload", methods=["GET"])
@cross_origin()
def fetch_hour_by_hour_workload():
    try:
        if request.method == "GET":
            today = []
            yesterday = []
            prior = datetime.strptime(get_current_datetime("Date"), "%m/%d/%y") - timedelta(days=1)
            prior = datetime.strftime(prior, "%m/%d/%y")
            for i in Hour_By_Hour_Workload.query.all():
                if (i.date).split()[0] == get_current_datetime("Date"):
                    obj = {
                        "Time": (i.date).split()[1][0:5],
                        "Workload": i.workload
                    }
                    today.append(obj)
                elif (i.date).split()[0] == prior:
                    obj = {
                        "Time": (i.date).split()[1][0:5],
                        "Workload": i.workload
                    }
                    yesterday.append(obj)
            return jsonify({"Today" : today, "Yesterday" : yesterday})
        else:
            # method not allowed error
            return abort(405)
    except Exception as e:
        error_handler("Fetch Hour By Hour Workload", "Low", e)
        return e
