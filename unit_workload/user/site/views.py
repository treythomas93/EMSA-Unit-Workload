# imports
from flask import Blueprint, render_template, request, session, redirect, url_for
from flask_login import login_user, logout_user, login_required, current_user
from unit_workload.models import User, Live_Workload, Hour_By_Hour_Workload
from unit_workload.extensions import db, login_manager
from unit_workload.error import error_handler, get_current_datetime
from werkzeug.security import check_password_hash
import os

user_site = Blueprint("user_site", __name__)

login_manager.login_view = 'index'

path = os.getcwd()

@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))

@user_site.route("/")
def index():
    if current_user.is_authenticated:
        user = current_user.Username
        auth = True
        role = current_user.User_Role
        return render_template("dashboard.html", user=user, auth=auth, role=role)
    else:
        return render_template("index.html")

@user_site.route('/login', methods=['POST'])
def login():
    try:
        if request.method == "POST":
            username = request.form['username']
            password = request.form['password']
            user = User.query.filter_by(Username=username).first()
            if user:
                check_password = check_password_hash(user.Password, password)
                if check_password:
                    login_user(user, remember=True)
                    session.permanent = True
                    if 'next' in session:
                        next = session['next']
                        return redirect(next)
                    user = "{} {}".format(user.First_Name, user.Last_Name)
                    with open(f"{path}/Logins.txt", "a") as f:
                        f.write("{} logged in successfully at datetime: {}\n".format(
                            user, get_current_datetime(None)))
                    return redirect(url_for("user_site.dashboard"))
                else:
                    msg = "ERROR: Invalid Username and/or Password"
                    return render_template("index.html", msg=msg)
            else:
                msg = "ERROR: Invalid Username and/or Password"
                return render_template("index.html", msg=msg)
    except Exception as e:
        error_handler("LOGIN", "Low", e)
        msg = "ERROR: an error occured!"
        return render_template("index.html", msg=msg)

@user_site.route('/logout')
def logout():
    logout_user()
    msg = "Logged Out!"
    return render_template("index.html", msg=msg)

# dashboard
@user_site.route("/dashboard")
@login_required
def dashboard():
    user = current_user.Username
    auth = True
    role = current_user.User_Role
    return render_template("dashboard.html", user=user, auth=auth, role=role)

